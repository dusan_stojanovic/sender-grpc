package main

import (
	"context"
	"fmt"
	"sender-grpc/grpcclient"
	"sender-grpc/messages"
	"time"
)

func main() {
	message := messages.MessagesRequest{
		Content: "Hello World!!!",
	}

	grpcClient := grpcclient.InitGrpcClient()

	for i := 0; i < 10; i++ {
		response, err := grpcClient.CreateMessage(context.Background(), &message)
		if err != nil {
			fmt.Println("Error: ", err)
		}
		fmt.Println("Response status: ", response.Status)
		time.Sleep(5 * time.Second)
	}
}
