package grpcclient

import (
	"log"
	"sender-grpc/messages"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func InitGrpcClient() messages.MessagesGrpcClient {
	opts := grpc.WithTransportCredentials(insecure.NewCredentials())
	clientConn, err := grpc.Dial("localhost:8081", opts)
	if err != nil {
		log.Fatalf("gRPC Client starting error: %v", err)
	}

	return messages.NewMessagesGrpcClient(clientConn)
}
